package me.voidcrafted.chatcenter

import me.voidcrafted.pushbullet.EventStream
import org.json.JSONObject

class PushbulletListener(token : String) : EventStream(token) {
    val acceptableTypes = arrayOf("push")
    override fun onMessage(message: String?) {
        val messageObject = JSONObject(message)
        // If it isn't a type we're listening for, ignore
        if (!acceptableTypes.contains(messageObject.getString("type"))) return

        // Get push subsection
        val push = messageObject.getJSONObject("push")

        // If it's not a messenger notification, ignore
        if (push.getString("type") != "mirror") return
        if (push.getString("application_name") != "Messenger") return

        val center = JDASingleton.jda.getGuildById("469899138125922316")
        center.textChannels
                .filter { JSONObject(it.topic).getString("type") == "sms" }
                .filter { JSONObject(it.topic).getString("contact") ==  push.getString("title") }
                .forEach {
                    it.sendMessage("**${push.getString("title")}**: ${push.getString("body")}")
                }
    }
}