package me.voidcrafted.chatcenter

import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.OnlineStatus
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import org.json.JSONObject
import java.io.File


class DiscordSetup (token : String) : ListenerAdapter() {
    private val config = JSONObject(File("./config.json").readText())


    private val jda = JDABuilder(AccountType.BOT)
            .setToken(token)
            .setStatus(OnlineStatus.IDLE)
            .addEventListener(this)
            .buildAsync()

    override fun onReady(event: ReadyEvent) {
        JDASingleton.jda = jda
    }

    private val pushSender = PushbulletSender(config.getString("pushbullet"), config.getString("device"))

    override fun onMessageReceived (message : MessageReceivedEvent) {
        if (message.author.id == jda.selfUser.id) return
        val channelJson = JSONObject((message.channel as TextChannel).topic)
        if (channelJson.getString("type") == "sms") {
            pushSender.sendSms(channelJson.getString("number"), message.message.contentDisplay)
            println("${channelJson.getString("number")} <-- [#${message.channel.name} - ${message.author.name}] ${message.message.contentDisplay}")
        }
    }
}
