package me.voidcrafted.chatcenter

import org.json.JSONObject

class PushbulletSender(val token : String, val device : String) {
    var idenCache : String? = null
    val iden: String
        get() {
            if (idenCache == null) idenCache = khttp.get("https://api.pushbullet.com/v2/users/me", headers = mapOf(
                        "Access-Token" to token
                )).jsonObject.getString("iden")
            return idenCache!!
        }

    fun sendSms(number : String, message: String) {
        khttp.post(
                "https://api.pushbullet.com/v2/ephemerals",
                headers = mapOf("Access-Token" to token),
                json = JSONObject(mapOf(
                        "type" to "push",
                        "push" to JSONObject(mapOf(
                                "type" to "messaging_extension_reply",
                                "package_name" to "com.pushbullet.android",
                                "source_user_iden" to iden,
                                "target_device_iden" to device,
                                "conversation_iden" to number,
                                "message" to message
                        ))
                ))
        )
    }
}