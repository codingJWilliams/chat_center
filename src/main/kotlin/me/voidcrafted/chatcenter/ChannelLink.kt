package me.voidcrafted.chatcenter

import net.dv8tion.jda.core.entities.TextChannel

class ChannelLink (val channel : TextChannel, val username : String, val number : String)