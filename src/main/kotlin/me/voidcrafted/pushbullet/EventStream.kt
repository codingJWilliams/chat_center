package me.voidcrafted.pushbullet

import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import org.json.JSONObject
import java.lang.Exception
import java.net.URI


open class EventStream (token : String) : WebSocketClient(URI.create("wss://stream.pushbullet.com/websocket/$token")) {
    init {
        this.connect()
        this.connectionLostTimeout = 0 // PushBullet does not send pings so disconnects after 30 seconds without this
    }
    override fun onClose(code: Int, reason: String?, remote: Boolean) {
        println("Disconnected from websocket. Error code $code, Reason $reason, remote:$remote")
    }

    override fun onMessage(message: String?) = Unit // Stub to be replaced

    override fun onError(ex: Exception?) = Unit

    override fun onOpen(handshakedata: ServerHandshake?) {
        println("Connected to websocket")
    }
}